public class Eagle {
    // Fields 
    private String breed;
	private int speed;
	private double size;
	
	public Eagle(String breed, int speed, double size){
			this.breed = breed;
			this.speed = speed;
			this.size = size;
	}
	
	public void description(){
		System.out.println("The size of the " + breed + " is " + size + " meters");		
	}
	
	public void topSpeed(){
		System.out.println("The " + breed + " has a top speed of " + speed + " Km/H");		
	}
	
	//GET FOR BREED
	public String getBreed(){
		return this.breed;
	}
	
	//GET AND SET FOR SPEED	
	public void setSpeed(int speed){
		this.speed = speed;
	}
	
	public int getSpeed(){
		return this.speed;
	}
	
	//GET FOR SIZE
	public double getSize(){
		return this.size;
	}
}


