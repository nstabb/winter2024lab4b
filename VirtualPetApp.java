import java.util.Scanner;
public class VirtualPetApp {
    public static void main(String[] args) {
		Eagle[] eaglets = new Eagle[2];
		Scanner input = new Scanner(System.in);
		
		for(int i = 0; i < eaglets.length; i++){
			System.out.println("\nPlease enter the breed of eagle " + (i+1));		
			String breed = input.nextLine();
				
			System.out.println("Please enter the speed of the breed in Km/h");		
			int speed = input.nextInt();
			
			System.out.println("Please enter the size of the breed in meters");		
			double size = input.nextDouble();
			input.nextLine();
			
			eaglets[i] = new Eagle(breed, speed, size);
		}

		System.out.println("Set the speed of the last eagle: ");
		int speedSet = input.nextInt();
		input.nextLine();
		
		
		System.out.println(eaglets[eaglets.length-1].getBreed());
		System.out.println(eaglets[eaglets.length-1].getSpeed());
		System.out.println(eaglets[eaglets.length-1].getSize());
		
		eaglets[eaglets.length-1].setSpeed(speedSet);
		
		System.out.println(eaglets[eaglets.length-1].getBreed());
		System.out.println(eaglets[eaglets.length-1].getSpeed());
		System.out.println(eaglets[eaglets.length-1].getSize());
    }
}
